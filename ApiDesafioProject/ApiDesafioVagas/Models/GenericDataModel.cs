using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ApiDesafioVagas.Models
{
    public abstract class GenericDataModel : IData
    {
        [BsonId]
        public ObjectId Id { get; set; }
        public DateTime TimeStamp => Id.CreationTime;

        public GenericDataModel() 
        { 
            Id = ObjectId.GenerateNewId();
        }
    }
}