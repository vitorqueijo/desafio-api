using System;
namespace ApiDesafioVagas.Models
{
    public class ExceptionPath : Exception
    {
        public ExceptionPath()
        {
        }

        public ExceptionPath(string message) : base(message)
        {
        }
    }
}