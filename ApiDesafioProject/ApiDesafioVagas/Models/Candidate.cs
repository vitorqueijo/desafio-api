using System.Runtime.Serialization;
using Newtonsoft.Json;
using RestSharp.Deserializers;

namespace ApiDesafioVagas.Models
{
    ///<summary>
    /// Model para deserialização de dados de um Candidato
    ///</summary>
    public class Candidate : GenericDataModel
    {
        [JsonProperty("nome")]
        public string Nome { get; set; }

        [JsonProperty("profissao")]
        public string Profissao { get; set; }
        [JsonProperty("localizacao")]
        public string Localizacao { get; set; }

        [JsonProperty("nivel")]
        public int Nivel { get; set; }
    }
}