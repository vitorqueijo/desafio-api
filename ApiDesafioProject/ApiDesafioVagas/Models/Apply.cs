using System;
using System.Collections.Generic;

namespace ApiDesafioVagas.Models
{
    public class Apply : GenericDataModel
    {
        public string Nome { get; set; }
        public string Localizacao { get; set; }
        public int Nivel { get; set; }
        public string Profissao { get; set; }
        public int id_vaga { get; set; }
        public int score { get; set; }
    }
}