
using Newtonsoft.Json;

namespace ApiDesafioVagas.Models
{
    ///<summary>
    /// Model para deserialização de dados de uma Vaga
    ///</summary>
    public class Job : GenericDataModel
    {
        [JsonProperty("empresa")]
        public string Empresa { get; set; }
        [JsonProperty("titulo")]
        public string Titulo { get; set; }
        [JsonProperty("descricao")]
        public string Descricao { get; set; }
        [JsonProperty("localizacao")]
        public string Localizacao { get; set; }
        [JsonProperty("nivel")]
        public int Nivel { get; set; }
    }
}