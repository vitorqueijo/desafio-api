namespace ApiDesafioVagas.Models
{
    public enum Level
    {
        estagiario = 1,
        junior = 2,
        pleno = 3,
        senior = 4,
        especialista = 5,
    }
}