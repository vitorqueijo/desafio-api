using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ApiDesafioVagas.Models
{
    public interface IData
    {
        [BsonId]
        [BsonRepresentation(BsonType.String)]
        [BsonElement("id")]
        ObjectId Id { get; set; }
        DateTime TimeStamp { get; }
    }
}