using System.Linq;
using System;
using System.Collections.Generic;

namespace ApiDesafioVagas.Models
{
    /// <summary>
    /// Random Forest Atempt Excluded from project
    /// </summary>
    public class PathForest
    {   
        public List<Path> Paths {get; set;} = new List<Path>();

        public int Limit {get; set;}

        public PathForest(IEnumerable<(int, int)> seed, int destiny)
        {
            Limit = destiny;

            foreach (var t in seed)
            {
                Paths.Add(new Path(t.Item1, t.Item2));
            }
        }
        public void AddPath(Path p) => Paths.Add(p);

        public Path GetMinimumPath() =>  Paths.FirstOrDefault(x => x.Cost == Paths.Min(p => p.Cost));
    }

    public class Path
    {
        public List<int> Nodes {get; set;} = new List<int>();
        public double Cost {get; set;}

        public Path(int node, int cost)
        {
            Nodes.Add(node);
            Cost = cost;
        }

        public void Step(int nextNode, int cost)
        {
            if(Nodes.Contains(nextNode))
            {
                throw new ExceptionPath();
            }
            Nodes.Add(nextNode);
            Cost += cost;
        }

        public int Last()
        {
            return Nodes.Last();
        }
    }
}