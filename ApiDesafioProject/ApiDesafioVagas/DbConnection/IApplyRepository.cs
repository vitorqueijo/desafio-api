using ApiDesafioVagas.Models;

namespace ApiDesafioVagas.DbConnection
{
    internal interface IApplyRepository : IGenericRepository<Apply> { }
}