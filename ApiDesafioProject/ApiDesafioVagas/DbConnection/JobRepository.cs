using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiDesafioVagas.Models;
using MongoDB.Bson;
using Newtonsoft.Json;
using static ApiDesafioVagas.DbConnection.Metadata;

namespace ApiDesafioVagas.DbConnection
{
    public class JobRepository : GenericRepository<Job>, IJobRepository
    {
        public JobRepository(IJobDbContext ctx) : base(ctx) { }
        
        public async Task<OperationResult> InsertJobsAsync(string jobs)
        {
            try
            {
                var jobsToInsert = JsonConvert.DeserializeObject<IEnumerable<Job>>(jobs);
                foreach(var job in jobsToInsert)
                {
                    await Insert(job);
                }

                var added = await Get();
                added.ToList();

                var result = added.Select(x => (x.Id, x.Empresa, x.Titulo)).ToString();

                return new OperationResult($"Jobs Successfully Added: {result}");
            }
            catch(Exception ex)
            {
                return await Task<OperationResult>.Run(() => {
                    var err =  new OperationResult();
                    err._error = $"{ex.Message}";
                    err._status = false;

                    return err;
                });
            }
        }
    }
}