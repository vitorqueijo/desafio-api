using MongoDB.Driver;

namespace ApiDesafioVagas.DbConnection
{
    public interface IApplyDbContext
    {
        IMongoCollection<Apply> GetCollection<Apply>(string name);
    }
}