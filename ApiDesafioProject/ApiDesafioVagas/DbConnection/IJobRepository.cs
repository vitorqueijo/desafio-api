using ApiDesafioVagas.Models;

namespace ApiDesafioVagas.DbConnection
{
    public interface IJobRepository : IGenericRepository<Job>
    {
    }
}