using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using ApiDesafioVagas.Models;
using MongoDB.Bson;
using MongoDB.Driver;

namespace ApiDesafioVagas.DbConnection
{
    public abstract class GenericRepository<T> : IGenericRepository<T> where T : class, IData
    {
        protected readonly IJobDbContext _jobContext;
        protected readonly ICandidateDbContext _candidateContext;
        protected readonly IApplyDbContext _applyContext;
        protected IMongoCollection<T> _dbCollection;

        protected GenericRepository(IJobDbContext ctx)
        {
            _jobContext = ctx;
            _dbCollection = _jobContext.GetCollection<T>(typeof(T).Name);
        }

        protected GenericRepository(ICandidateDbContext ctx)
        {
            _candidateContext = ctx;
            _dbCollection = _candidateContext.GetCollection<T>(typeof(T).Name);
        }

        protected GenericRepository(IApplyDbContext ctx) 
        {
            _applyContext = ctx;
            _dbCollection = _applyContext.GetCollection<T>(typeof(T).Name);
        }

        public virtual async Task Insert(T obj)
        {
            await _dbCollection.InsertOneAsync(obj);
        }

        public virtual async Task<T> Get(string id)
        {
            return await _dbCollection.FindAsync(Builders<T>.Filter.Eq("_id", new ObjectId(id))).Result.FirstOrDefaultAsync();
        }

        public virtual async Task<IEnumerable<T>> Get() 
        {
            var result = await _dbCollection.FindAsync(Builders<T>.Filter.Empty);
            return await result.ToListAsync();
        }
    }
}