using MongoDB.Driver;

namespace ApiDesafioVagas.DbConnection
{
    public interface IJobDbContext
    {
        IMongoCollection<Job> GetCollection<Job>(string name);
    }
}