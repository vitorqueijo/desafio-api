using ApiDesafioVagas.Models;

namespace ApiDesafioVagas.DbConnection
{
    public interface ICandidateRepository : IGenericRepository<Candidate> { }
}