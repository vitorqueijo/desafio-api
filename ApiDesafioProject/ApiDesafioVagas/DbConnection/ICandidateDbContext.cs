using MongoDB.Driver;

namespace ApiDesafioVagas.DbConnection
{
    public interface ICandidateDbContext
    {
        IMongoCollection<Candidate> GetCollection<Candidate>(string name);
    }
}