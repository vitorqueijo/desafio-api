using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiDesafioVagas.CoreMatrixJobs;
using ApiDesafioVagas.Models;
using Microsoft.AspNetCore.Mvc;
using static ApiDesafioVagas.DbConnection.Metadata;

namespace ApiDesafioVagas.DbConnection
{
    public class ApplyRepository : GenericRepository<Apply>, IApplyRepository
    {
        public ApplyRepository(IApplyDbContext ctx) : base(ctx) { }

        public async Task<OperationResult> InsertApply(Score apply)
        {
            var result = apply.GetApply();
            var checkApply = await Get(result.id_vaga.ToString());
            if(checkApply != null)
            {
                return await Task<OperationResult>.Run(() => {
                    return new OperationResult($"Apply already registred!");
                });
            }
            await Insert(result);
            return new OperationResult($"Apply '{result.Id}' successfully added to '{result.id_vaga}'");
        }

        public async Task<JsonResult> GetRanking()
        {
            var result = await Get();
            result.ToList().OrderByDescending(x => x.score);
            return await Task<JsonResult>.Run(() => {
                return new JsonResult(result);
            });
        }
    }
}