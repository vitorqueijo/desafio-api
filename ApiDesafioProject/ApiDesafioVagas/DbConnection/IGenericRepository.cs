using System.Collections.Generic;
using System.Threading.Tasks;
using ApiDesafioVagas.Models;

namespace ApiDesafioVagas.DbConnection
{
    public interface IGenericRepository<T> where T : class, IData
    {
        Task Insert(T obj);
        Task<T> Get(string id);
        Task<IEnumerable<T>> Get();
    }
}