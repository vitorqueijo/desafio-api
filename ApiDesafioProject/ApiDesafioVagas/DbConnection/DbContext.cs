using ApiDesafioSystem;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace ApiDesafioVagas.DbConnection
{
    public class JobDbContext : IJobDbContext
    {
        private IMongoDatabase _db { get; set; }
        private MongoClient _mongoClient { get; set; }

        public IClientSessionHandle Session { get; set; }

        public JobDbContext(IOptions<MongoDbSettings> config)
        {
            _mongoClient = new MongoClient(config.Value.ConnectionString);
            _db = _mongoClient.GetDatabase(config.Value.DatabaseName);
        }

        public IMongoCollection<T> GetCollection<T>(string name)
        {
            return _db.GetCollection<T>(name);
        }
    }

    public class CandidateDbContext : ICandidateDbContext
    {
        private IMongoDatabase _db { get; set; }
        private MongoClient _mongoClient { get; set; }

        public IClientSessionHandle Session { get; set; }

        public CandidateDbContext(IOptions<MongoDbSettings> config)
        {
            _mongoClient = new MongoClient(config.Value.ConnectionString);
            _db = _mongoClient.GetDatabase(config.Value.DatabaseName);
        }

        public IMongoCollection<T> GetCollection<T>(string name)
        {
            return _db.GetCollection<T>(name);
        }
    }

    public class ApplyDbContext : IApplyDbContext
    {
        private IMongoDatabase _db { get; set; }
        private MongoClient _mongoClient { get; set; }

        public IClientSessionHandle Session { get; set; }

        public ApplyDbContext(IOptions<MongoDbSettings> config)
        {
            _mongoClient = new MongoClient(config.Value.ConnectionString);
            _db = _mongoClient.GetDatabase(config.Value.DatabaseName);
        }

        public IMongoCollection<T> GetCollection<T>(string name)
        {
            return _db.GetCollection<T>(name);
        }
    }
}