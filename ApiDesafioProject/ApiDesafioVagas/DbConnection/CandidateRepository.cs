using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ApiDesafioVagas.Models;
using Newtonsoft.Json;
using static ApiDesafioVagas.DbConnection.Metadata;
using System.Linq;
using ApiDesafioVagas.CoreMatrixJobs;
using Microsoft.AspNetCore.Components;

namespace ApiDesafioVagas.DbConnection
{
    public class CandidateRepository : GenericRepository<Candidate>, ICandidateRepository
    {
        public CandidateRepository(ICandidateDbContext ctx) : base(ctx) { }

        #region inject dependecy
        [Inject]
        private JobRepository _jobRepository { get; set; }

        private ApplyRepository _applyRepository { get; set; }
        #endregion

        private Score _score;

        public async Task<OperationResult> InsertCandidatesAsync(string candidates)
        {
            try
            {
                var candsToInsert = JsonConvert.DeserializeObject<IEnumerable<Candidate>>(candidates);
                foreach(var cand in candsToInsert)
                {
                    await Insert(cand);
                }
                var added = await Get();
                added.ToList();

                var result = added.Select(x => (x.Id, x.Nome)).ToString();

                return new OperationResult($"Candidates Successfully Added: {result} ");
            }
            catch(Exception ex)
            {
                return await Task<OperationResult>.Run(() => {
                    var err =  new OperationResult();
                    err._error = $"{ex.Message}";
                    err._status = false;

                    return err;
                });
            }
        }

        public async Task<OperationResult> Apply(string apply)
        {
            var applyJson = JsonConvert.DeserializeObject<ApplyJson>(apply);
            var candidate = await Get(applyJson.id_pessoa);
            var job = await _jobRepository.Get(applyJson.id);
            if( job != null || candidate != null )
            {
                return await Task<OperationResult>.Run(() => {
                    return new OperationResult($"Job or Candidate's id are invalid!");
                });
            }
            _score = new Score(candidate, job);
            return await _applyRepository.InsertApply(_score);
        }
    }
}