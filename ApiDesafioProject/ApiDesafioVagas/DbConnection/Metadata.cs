using System;
using Newtonsoft.Json;

namespace ApiDesafioVagas.DbConnection
{
    public class Metadata
    {
        public class OperationResult<T> : OperationResult
        {
            public virtual T _result { get; set; }
            public virtual string[] errors { get; set; }

            public OperationResult(T result, bool status, string message=null)
            {
                if(!status && message != null)
                {
                    throw new Exception($"Failured due to: '{message}'.");
                }
                else if(!status)
                {
                    throw new Exception($"Unknow error.");
                }
                _result = result;
                _status = status;
            }
        }
        public class OperationResult
        {
            public bool _status { get; set; }
            public string _error { get; set; }

            public OperationResult()
            {
                _status = true;
            }

            public OperationResult(string error)
            {
                _status = false;
                _error = error;
            }

            public static OperationResult<T> OkResult<T>(T result)
            {
                return new OperationResult<T>(result, true);
            }
        }

        public class ApplyJson 
        {
            [JsonProperty("id_vaga")]
            public string id { get; set; }
            [JsonProperty("id_pessoa")]
            public string id_pessoa { get; set; }
            
        }
    }
}