using System.Runtime.Intrinsics.X86;
using System;
using System.Collections.Generic;
using System.Linq;
using ApiDesafioVagas.Extensions;
using ApiDesafioVagas.Models;

namespace ApiDesafioVagas.CoreMatrixJobs
{
    public class MinimumCostDistance
    {
        private int _cost { get; set; }
        private string _origin { get; set; }
        private string _final { get; set; }

        public MinimumCostDistance(string origin, string final)
        {
            _origin = origin;
            _final = final;
        }

        public int Run() => GetMinimumCost();

        #region Graph Weights
        private static readonly int[][] cost_matrix = 
        { 
            new int[] {0, 5, 0, 0, 0, 0},
            new int[] {5, 0, 7, 3, 0, 0},
            new int[] {0, 7, 0, 0, 4, 0}, 
            new int[] {0, 3, 0, 0, 10, 8}, 
            new int[] {0, 0, 4, 10, 0, 0}, 
            new int[] {0, 0, 0, 8, 0, 0}
        };
        #endregion
        #region MinimumPathCostFinder
        private int GetMinimumCost()
        {
            var indexOrigin = ExtensionMethods.GetLocationValue(_origin);
            var indexFinal = ExtensionMethods.GetLocationValue(_final);

            if (indexOrigin == default || indexFinal == default)
            {
                return default;
            }

            if (indexFinal == indexOrigin)
            {
                return 0;
            }

            return Cost(indexOrigin, indexFinal);
        }
        #region Recursive Methods
        /// <summary>
        /// Method to find the minimum path cost
        /// </summary>
        /// <param name="indexOrigin">origin node's index</param>
        /// <param name="indexFinal">final node's index</param>
        /// <returns>minimum cost</returns>
        private int Cost(int indexOrigin, int indexFinal)
        {
            int[] pathMemory = new int[] { };
            var cost = 0;
            var paths = Cost(indexOrigin, cost, pathMemory, indexFinal);
            return paths.Where(x => x != default).Min(x => x);
        }

        /// <summary>
        /// Recursive Function to find all path's costs from Origin node to final node
        /// </summary>
        /// <param name="indexOrigin">origin node's index</param>
        /// <param name="cost">Minimum cost value</param>
        /// <param name="pathMemory">path's "Memory"</param>
        /// <param name="indexFinal">final node's index</param>
        /// <returns>List of costs</returns>
        private IEnumerable<int> Cost(int indexOrigin, int cost, int[] pathMemory, int indexFinal) 
        {
            var vector = cost_matrix[indexOrigin];

            pathMemory.Append(indexOrigin);

            for(var i=0; i<vector.Length; i++)
            {
                if(pathMemory.Contains(i))
                {
                    yield return default;
                }
                
                if(!IsZero(vector, indexFinal))
                {
                    yield return cost;
                }

                if(!IsZero(vector, i)) 
                {
                    cost += vector[i];
                    foreach(var result in Cost(i, cost, pathMemory, indexFinal))
                    { 
                        yield return result; 
                    }
                }
            }
        }
        #endregion

        #region Aux Methods
        private static bool IsZero(int[] vector, int index)
        {
            return Int32.Equals(vector[index], 0);
        }
        #endregion
        #endregion
    }
}