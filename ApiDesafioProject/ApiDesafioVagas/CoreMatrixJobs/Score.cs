using System.Linq;
using System;
using ApiDesafioVagas.Models;

namespace ApiDesafioVagas.CoreMatrixJobs
{
    public sealed class Score
    {
        private Apply _apply = new Apply();
        private static Candidate _candidate;
        private static Job _job;
        private static MinimumCostDistance _minCost;

        public Score(Candidate candidate, Job job)
        {
            _candidate = candidate;
            _job = job;
            _minCost = new MinimumCostDistance(candidate.Localizacao, job.Localizacao);
        }

        private int _distanceCost => _minCost.Run();
        private int _levelParameter => (100 - 25*Math.Abs(_job.Nivel - _candidate.Nivel));
        private int _distanceParameter => _distanceConvert(_distanceCost < 5 ? 1 : _distanceCost / 5);
        Func<int,int> _distanceConvert = (x) => -25*x + 125; 

        private int GetScore() => (_levelParameter + _distanceParameter) / 2;

        public Apply GetApply()
        {
            _apply.Nome = _candidate.Nome;
            _apply.Profissao = _candidate.Profissao;
            _apply.Localizacao = _candidate.Localizacao;
            _apply.Nivel = _candidate.Nivel;
            _apply.score = GetScore();
            return _apply;
        }
    }
}