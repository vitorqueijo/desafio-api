using System.Threading.Tasks;
using ApiDesafioVagas.DbConnection;
using ApiDesafioVagas.Models;
using Microsoft.AspNetCore.Mvc;
using static ApiDesafioVagas.DbConnection.Metadata;

namespace ApiDesafioVagas.ApiController
{
    [Route("v1")]
    [ApiController]
    public class ApiController : Controller
    {
        private readonly JobRepository _job;
        private readonly CandidateRepository _candidate;

        private readonly ApplyRepository _applyRepo;

        public ApiController(JobRepository jobRepo, CandidateRepository candidateRepo, ApplyRepository applyRepo)
        {
            _job = jobRepo;
            _candidate = candidateRepo;
            _applyRepo = applyRepo;
        }

        [HttpPost("vagas")]
        public async Task<OperationResult> Register([FromBody]string jobs)
        {
            return await _job.InsertJobsAsync(jobs);
        }
        
        [HttpPost("pessoas")]
        public async Task<OperationResult> RegisterCandidates([FromBody]string candidates)
        {
            return await _candidate.InsertCandidatesAsync(candidates);
        }

        [HttpPost("candidaturas")]
        public async Task<OperationResult> ApplyCandidateToJob([FromBody]string apply)
        {
            return await _candidate.Apply(apply);
        }

        [HttpGet("candidaturas/ranking")]
        public async Task<JsonResult> GetRanking()
        {
            return Json(await _applyRepo.GetRanking());
        }
        
    }
}