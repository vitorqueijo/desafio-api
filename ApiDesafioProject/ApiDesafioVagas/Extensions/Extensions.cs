using System;
using System.Collections.Generic;
using System.Linq;
using ApiDesafioVagas.Models;

namespace ApiDesafioVagas.Extensions
{
    public class Pair<T1, T2>
    {
        public T1 First { get; set; }
        public T2 Second { get; set; }

        public Pair(T1 first, T2 second)
        {
            First = first;
            Second = second;
        }
    }
    public static class ExtensionMethods
    {
        public static int GetLocationValue(string locationString)
        {
            try
            {
                return (int)Enum.Parse(typeof(Location), locationString, true);
            }
            catch
            {
                return default;
            }
        }
    }
}